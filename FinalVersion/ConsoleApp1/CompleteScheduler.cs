﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using SimCPULibrary;

namespace Final
{
    class CompleteScheduler : Scheduler
    {
        int counter;

        Queue<(Registers register, SimulatedProcess process)> highPriority;
        Queue<(Registers register, SimulatedProcess process)> normalPriority;
        Queue<(Registers register, SimulatedProcess process)> lowPriority;

        const int HIGH = 2;
        const int NORMAL = 7;
        const int LOW = 19;


        public CompleteScheduler(CPU[] cpus, TimeSpan tick, (SimulatedProgram program, Priority priority, CPU[] affinities)[] programs) : base(cpus, tick, programs)
        {
            counter = new Random().Next(HIGH + 1, LOW - 1); //This is just for fun

            highPriority = new Queue<(Registers register, SimulatedProcess process)>();
            normalPriority = new Queue<(Registers register, SimulatedProcess process)>();
            lowPriority = new Queue<(Registers register, SimulatedProcess process)>();

            foreach (SimulatedProcess sp in processes) //Dividing processes into separated queues by priority
            {
                if (sp.ProcessPriority == Priority.High)
                {
                    highPriority.Enqueue((new Registers(), sp));
                }
                else if (sp.ProcessPriority == Priority.Normal)
                {
                    normalPriority.Enqueue((new Registers(), sp));
                }
                else
                {
                    lowPriority.Enqueue((new Registers(), sp));
                }
            }

        }

        public override void SchedulerInterrupt()
        {
            //TO_DO
            //Check affinities
            //Save context
            //Input new context
            //OD_OT


            foreach (CPU cpu in cpus)
            {

                //Check if there is any process with affinitie set to this cpu
                if (highPriority.Any(proc => proc.process.Affinities.Contains(cpu)) || normalPriority.Any(proc => proc.process.Affinities.Contains(cpu)) || lowPriority.Any(proc => proc.process.Affinities.Contains(cpu)))
                {

                    if (cpu.CurrentProcess != null && !cpu.CurrentProcess.Completed) //This part saves context
                    {
                        if (cpu.CurrentProcess.ProcessPriority == Priority.High)
                        {
                            highPriority.Enqueue((cpu.Registers, cpu.CurrentProcess));
                        }
                        else if (cpu.CurrentProcess.ProcessPriority == Priority.Normal)
                        {
                            normalPriority.Enqueue((cpu.Registers, cpu.CurrentProcess));
                        }
                        else
                        {
                            lowPriority.Enqueue((cpu.Registers, cpu.CurrentProcess));
                        }
                    } //Here it should be saved

                    //Here is part that inputs new process on CPU


                    if (((counter %= LOW) == 0) && lowPriority.Count != 0)
                    {
                        (cpu.Registers, cpu.CurrentProcess) = lowPriority.Dequeue();
                    }
                    else if ((counter % NORMAL == 0) && normalPriority.Count != 0)
                    {
                        (cpu.Registers, cpu.CurrentProcess) = normalPriority.Dequeue();
                    }
                    else if (highPriority.Count != 0)
                    {
                        (cpu.Registers, cpu.CurrentProcess) = highPriority.Dequeue();
                    }
                    else if (normalPriority.Count != 0)
                    {
                        (cpu.Registers, cpu.CurrentProcess) = normalPriority.Dequeue();
                    }
                    else if (lowPriority.Count != 0)
                    {
                        (cpu.Registers, cpu.CurrentProcess) = lowPriority.Dequeue();
                    }

                    counter++;

                }
            }
        }
    }
}
