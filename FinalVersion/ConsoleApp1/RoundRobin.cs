﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using SimCPULibrary;

namespace Final
{
    class RoundRobin : Scheduler
    {

        Queue<(Registers register, SimulatedProcess process)> context;
       
        public RoundRobin(CPU[] cpus, TimeSpan tick,  (SimulatedProgram program, Priority priority, CPU[] affinities)[] programs) : base(cpus, tick, programs)
        {
            context = new Queue<(Registers register, SimulatedProcess process)>();
            foreach(SimulatedProcess sp in processes)
            {
                context.Enqueue((new Registers(), sp));
            }
        }

        public override void SchedulerInterrupt()
        {

            

            foreach(CPU cpu in cpus)
            {
                //nullpointer? completed?
                //Save context
                if(cpu.CurrentProcess!=null && !cpu.CurrentProcess.Completed)
                {
                    context.Enqueue((cpu.Registers, cpu.CurrentProcess));
                }

                if(context.Count != 0) //Input context
                {
                    (cpu.Registers, cpu.CurrentProcess) = context.Dequeue();
                }


            }

        }
    }
}
