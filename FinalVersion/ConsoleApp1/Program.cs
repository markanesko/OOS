﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SimCPULibrary;
using DllConsoleTest;

namespace Final
{
    class MainClass
    {
        static void Main(string[] args)
        {
            CPU cpu1 = new CPU(1, TimeSpan.FromMilliseconds(100));
            CPU cpu2 = new CPU(2, TimeSpan.FromMilliseconds(100));
            CPU[] cpus = { cpu1, cpu2 };

            SimulatedProgram program1 = new SimulatedProgram(new object[]
          {
              (OpCode.Mov, RegCode.A, "KONJ"),
              (OpCode.PrintLn),
              (OpCode.Jmp, 0)

          });
            

            SimulatedProgram program2 = new SimulatedProgram(new object[]
          {
              (OpCode.Mov, RegCode.B, 1),
              (OpCode.Add, RegCode.B, 1),
              (OpCode.PrintLn, RegCode.B),
              (OpCode.Jmp, 1)

          });

            SimulatedProgram program3 = new SimulatedProgram(new object[]
           {
               (OpCode.Mov, RegCode.C, 4),
               (OpCode.PrintLn, RegCode.C),
               (OpCode.Add, RegCode.C, -1),
               (OpCode.Jnz, 1)

           });
            SimulatedProgram program4 = new SimulatedProgram(new object[]
          {
              (OpCode.Mov, RegCode.A, "MIS"),
              (OpCode.PrintLn),
              (OpCode.Jmp, 0)

          });


            (SimulatedProgram program, Priority priority, CPU[] affinities)[] programs = new(SimulatedProgram, Priority, CPU[])[6];

            programs[0] = (program1, Priority.High, new CPU[] { cpu1, cpu2});
            programs[1] = (program4, Priority.Normal, new CPU[] {  cpu2 });
            programs[2] = (program3, Priority.Low, new CPU[] { cpu2 });
            programs[3] = (program2, Priority.Low, new CPU[] {  cpu2 });
            programs[4] = (program3, Priority.Normal, new CPU[] {  cpu2 });
            programs[5] = (program3, Priority.Low, new CPU[] {  cpu2 });

           

            Scheduler s = new AgeingPriorityScheduler(new CPU[] { cpu1, cpu2 }, TimeSpan.FromMilliseconds(200), programs);
            s.Run();
        }
    }
}
