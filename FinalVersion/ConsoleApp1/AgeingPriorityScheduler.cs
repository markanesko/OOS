﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using SimCPULibrary;

namespace Final
{
    class AgeingPriorityScheduler : Scheduler
    {

        int ageing;

        Queue<(Registers registers, SimulatedProcess process)> highPriority;
        Queue<(Registers registers, SimulatedProcess process)> otherPriority;
        
        const int ageingLow = 7;
        const int ageingNormal = 3;
        
        public AgeingPriorityScheduler(CPU[] cpus, TimeSpan tick, (SimulatedProgram program, Priority priority, CPU[] affinities)[] programs) : base(cpus, tick, programs)
        {
            ageing = 0;

            highPriority = new Queue<(Registers register, SimulatedProcess process)>();
            otherPriority = new Queue<(Registers register, SimulatedProcess process)>();


            foreach (SimulatedProcess sp in processes)
            {
                if (sp.ProcessPriority == Priority.High)
                {
                    highPriority.Enqueue((new Registers(), sp));
                }
                else
                {
                    otherPriority.Enqueue((new Registers(), sp));
                }


            }
        }

        public override void SchedulerInterrupt()
        {
            foreach (CPU cpu in cpus)
            {
                if (cpu.CurrentProcess != null && !cpu.CurrentProcess.Completed) //This part saves context
                {
                    if (cpu.CurrentProcess.ProcessPriority == Priority.High)
                    {
                        highPriority.Enqueue((cpu.Registers, cpu.CurrentProcess));
                    }
                    else
                    {
                        otherPriority.Enqueue((cpu.Registers, cpu.CurrentProcess));
                    }

                }

                //Here is part that inputs new pcbs

                if (highPriority.Count != 0)
                {
                    (cpu.Registers, cpu.CurrentProcess) = highPriority.Dequeue();
                }

                if (otherPriority.Count != 0)
                {
                    (Registers r, SimulatedProcess sp) el = otherPriority.Dequeue();
                    if (el.sp.ProcessPriority == Priority.Low && (ageing %= ageingLow) == 0)
                    {
                        highPriority.Enqueue(el);
                    }
                    else if (ageing % ageingNormal == 0)
                    {
                        highPriority.Enqueue(el);
                    }
                    else
                    {
                        otherPriority.Enqueue(el);
                    }
                }



                ageing++;

            }

        }
    }
}
