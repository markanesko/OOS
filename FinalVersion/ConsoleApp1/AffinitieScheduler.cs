﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using SimCPULibrary;

namespace Final
{
    class AffinitieScheduler : Scheduler
    {

        List<(Registers register, SimulatedProcess process)> context;

        public AffinitieScheduler(CPU[] cpus, TimeSpan tick, (SimulatedProgram program, Priority priority, CPU[] affinities)[] programs) : base(cpus, tick, programs)
        {
            context = new List<(Registers register, SimulatedProcess process)>();
            foreach (SimulatedProcess sp in processes)
            {
                context.Add((new Registers(), sp));
            }

        }

        public override void SchedulerInterrupt()
        {
            foreach (CPU cpu in cpus)
            {


                //nullpointer? completed?
                //Save context
                if (cpu.CurrentProcess != null && !cpu.CurrentProcess.Completed) //Take what is on CPU and put it in List
                {
                    context.Append((cpu.Registers, cpu.CurrentProcess));
                    
                }

                if (context.Count != 0) //Input context
                {

                    foreach ((Registers reg, SimulatedProcess sp) el in context) //Find process and take it from List and put it on CPU
                    {
                        if (el.sp.Affinities.Contains(cpu))
                        {
                            (cpu.Registers, cpu.CurrentProcess) = context.ElementAt(context.IndexOf(el)); // dequeue part 1
                            context.RemoveAt(context.IndexOf(el)); // dequeue part 2
                            break;
                        }
                    }

                }

            }

        }
    }
}
