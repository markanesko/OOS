﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DokanNet;



namespace FileSistem
{
    class Program
    {
        static void Main(string[] args)
        {
            WinNTFS fs = new WinNTFS();
            fs.Mount("r:\\", DokanOptions.DebugMode | DokanOptions.StderrOutput);
        }
    }
}
