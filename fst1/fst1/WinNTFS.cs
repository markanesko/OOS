﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;



using DokanNet;

using Sop.Collections.Generic.BTree;

namespace FileSistem
{
    class WinNTFS : IDokanOperations
    {
        const string EXTENSION_DELIMITER = "."; // This is not necessary
        const string DELIMITER = "\\"; 
        const string MOUNT_POINT_NAME = "WinNTFS";  // 
        const string FILE_SYSTEM_NAME = "Laboratory File System - NTFS"; //

        const int DEGREE = 5; // int => Deegree of BTree (currently not needed)
        const int DEPTH = 12; // int => Depth of File System
        const int NAME_SIZE = 25; // int => Maximum number of characters in file name
        const int MAX_NUMBER_FILES = 16; // int => Maximum number of files in specific folder
        const int EXTENSION_LENGTH = 4; // int => Legal length of file extension  ++++++++++++++ . IS INCLUDED IN EXTENSION!!!!!!!!
        const int MAX_FILE_SIZE = 32; // It's in MB => Maximum size of file on file system
        const int FILE_SYSTEM_CAPACITY = 512; // It's in MB => File system capacity
        const int KB = 1024; // Just one int to represent Kilo Bytes
        const int MB = 1048576; // And one int to represent Mega Bytes

        //Journaling stuf
        const int INITIAL_QUEUE_LENGHT = 576 ; // MAX_NUMBER_FILES * DEPTH * 3
        const int FINISHED = 1;
        const int START = 0;


        BTreeDictionary<string, byte[]> files = new BTreeDictionary<string, byte[]>();
        HashSet<String> directorys = new HashSet<string>();

        Queue<ValueTuple<string, string, int>> journal = new Queue<(string, string, int)>(INITIAL_QUEUE_LENGHT);

        long bytes = 0;

        public void Cleanup(string fileName, DokanFileInfo info)
        {
            if (info.DeleteOnClose && info.Context == null)
            {
                if (IsDirectory(fileName))
                {
                    directorys.Remove(fileName); // It is directory, so just delete it

                    foreach(var x in directorys.Where(x => x.StartsWith(fileName + DELIMITER))) // Delete all sub directorys
                    {
                        directorys.Remove(x);
                    }

                    foreach (var x in files.Keys.Where(x => x.StartsWith(fileName + DELIMITER))) // Delete all files that are contained in directory
                    {
                        bytes -= files[x].Length;
                        files.Remove(x);

                    }
                }
                else
                {
                    bytes -= files[fileName].Length;
                    files.Remove(fileName); // This is safe
                }

            }

        }

        public void CloseFile(string fileName, DokanFileInfo info)
        {
        }


        //TO_DO
        public NtStatus CreateFile(string fileName, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info)
        {

            
           
            if (fileName == DELIMITER)
                return NtStatus.Success;
            if (access == DokanNet.FileAccess.ReadAttributes && mode == FileMode.Open)
                return NtStatus.Success;

            

            if (mode == FileMode.CreateNew)
            {
                if (attributes == FileAttributes.Directory || info.IsDirectory)
                {
                    if (GetCurrentDepth(fileName) < DEPTH && Path.GetDirectoryName(fileName).Length <= NAME_SIZE && !directorys.Contains(fileName))
                    {
                        directorys.Add(fileName);
                    }
                    else if(directorys.Contains(fileName))
                    {
                        return DokanResult.AlreadyExists;
                    }
                    else
                    {
                        return DokanResult.InvalidName;
                    }


                }
                else if (!files.Keys.Contains(fileName))
                {

                    String dirName = Path.GetDirectoryName(fileName);
                    

                    int countDirElements = this.files.Keys.Where(x => x.StartsWith(dirName)).Count();

                   
                    if (GetCurrentDepth(fileName) <= DEPTH && Path.GetFileNameWithoutExtension(fileName).Length <= NAME_SIZE && countDirElements <= MAX_NUMBER_FILES && Path.GetExtension(fileName).Length == EXTENSION_LENGTH && !files.Keys.Contains(fileName))
                    {
                        files.Add(fileName, new byte[0]);
                    }
                    else if (files.Keys.Contains(fileName))
                    {
                        return DokanResult.AlreadyExists;
                    }
                    else
                    {
                        return DokanResult.InvalidName;
                    }
                }
            }
            return NtStatus.Success;
        }
        //OD_OT

        //TO_DO
        public NtStatus DeleteDirectory(string fileName, DokanFileInfo info)
        {

            if (!info.IsDirectory)
                return DokanResult.Error; // If selected item is not directory return error

            return DokanResult.Success; // Mark it for deletion
        }
        //OD_OT

        //TO_DO
        public NtStatus DeleteFile(string fileName, DokanFileInfo info)
        {
            if (info.IsDirectory)
                return DokanResult.Error;


            return DokanResult.Success; // Mark it for deletion
        }
        //OD_OT

        //TO_DO
        public NtStatus FindFiles(string fileName, out IList<FileInformation> files, DokanFileInfo info)
        {
            files = new List<FileInformation>();
            int path = GetCurrentDepth(fileName) + 1;

            if(fileName == DELIMITER)
            {
                foreach (var x in directorys.Where(x => GetCurrentDepth(x) == 1))
                {
                    FileInformation fi = new FileInformation();
                    fi.Attributes = FileAttributes.Directory;
                    fi.FileName = Path.GetFileName(x);
                    files.Add(fi);
                }

                foreach (var x in this.files.Where(x => GetCurrentDepth(x.Key) == 1))
                {
                    FileInformation fi = new FileInformation();
                    fi.FileName = Path.GetFileName(x.Key);
                    fi.Length = x.Value.Length;
                    files.Add(fi);
                }

                return NtStatus.Success;
            }

            //foreach (var x in directorys.Where(x => x.StartsWith(fileName) && x.Length > fileName.Length))
            foreach (var x in directorys.Where(x => x.StartsWith(fileName + DELIMITER) && x.Length > fileName.Length))
            {
                FileInformation fi = new FileInformation();
                fi.Attributes = FileAttributes.Directory;
                fi.FileName = Path.GetFileName(x);
                files.Add(fi);
            }
            foreach (var x in this.files.Where(x => x.Key.StartsWith(fileName) && GetCurrentDepth(x.Key) == path))
            {
                FileInformation fi = new FileInformation();
                fi.FileName = Path.GetFileName(x.Key);
                fi.Length = x.Value.Length;
                files.Add(fi);
            }

            return NtStatus.Success;
        }
        //OD_OT

        public NtStatus FindFilesWithPattern(string fileName, string searchPattern, out IList<FileInformation> files, DokanFileInfo info)
        {
            files = new FileInformation[0];
            return DokanResult.NotImplemented;
        }

        public NtStatus FindStreams(string fileName, out IList<FileInformation> streams, DokanFileInfo info)
        {
            streams = new FileInformation[0];
            return DokanResult.NotImplemented;
        }

        public NtStatus FlushFileBuffers(string fileName, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus GetDiskFreeSpace(out long freeBytesAvailable, out long totalNumberOfBytes, out long totalNumberOfFreeBytes, DokanFileInfo info)
        {

            totalNumberOfBytes = FILE_SYSTEM_CAPACITY * MB;
            freeBytesAvailable = totalNumberOfBytes - bytes;
            totalNumberOfFreeBytes = freeBytesAvailable;
            return NtStatus.Success;
        }

        //TO_DO
        public NtStatus GetFileInformation(string fileName, out FileInformation fileInfo, DokanFileInfo info)
        {
            
            if (directorys.Contains(fileName))
            {
                fileInfo = new FileInformation()
                {
                    FileName = Path.GetFileName(fileName),
                    Attributes = FileAttributes.Directory,
                    CreationTime = null,
                    LastWriteTime = null
                };

            }
            else if (files.ContainsKey(fileName))
            {
                byte[] file = files[fileName];

                fileInfo = new FileInformation()
                {
                    FileName = Path.GetFileName(fileName),
                    Length = file.Length,
                    Attributes = FileAttributes.Normal,
                    CreationTime = DateTime.Now,
                    LastWriteTime = DateTime.Now
                };
            }
            else
            {
                fileInfo = default(FileInformation);
                return NtStatus.Error;
            }

            return NtStatus.Success;
        }
        //OD_OT

        public NtStatus GetFileSecurity(string fileName, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            security = null;
            return NtStatus.Error;
        }

        //TO_DO
        public NtStatus GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features, out string fileSystemName, DokanFileInfo info)
        {
            volumeLabel = MOUNT_POINT_NAME;
            features = FileSystemFeatures.None;
            fileSystemName = FILE_SYSTEM_NAME;

            return NtStatus.Success;
        }
        //OD_OT

        public NtStatus LockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus Mounted(DokanFileInfo info)
        {
            return NtStatus.Success;
        }

        //TO_DO
        public NtStatus MoveFile(string oldName, string newName, bool replace, DokanFileInfo info)
        {

            if(Path.GetFileNameWithoutExtension(newName).Length >= NAME_SIZE)
            {
                return NtStatus.Error;
            }


            if (oldName != newName && oldName != null && newName != null)
            {
                if (info.IsDirectory || IsDirectory(oldName) )
                {

                    //directorys.Where(x => x.StartsWith(oldName)).Select(x => x.Replace(oldName, newName));

                    directorys.Remove(oldName);
                    directorys.Add(newName);


                    foreach (var x in directorys)
                    {
                        string temp = x;
                        if (temp.StartsWith(oldName + DELIMITER))
                        {
                            directorys.Remove(temp);
                            temp = temp.Replace(oldName, newName);
                            directorys.Add(temp);
                        }

                    }

                    //files.Keys.Where(x => x.StartsWith(oldName)).Select(x => x = x.Replace(oldName, newName));

                    foreach (var x in files.Keys)
                    {
                        string temp = x;
                        if (temp.StartsWith(oldName + DELIMITER))
                        {
                            byte[] byteTemp = files[temp];
                            files.Remove(temp);
                            temp = temp.Replace(oldName, newName);
                            files.Add(temp, byteTemp);
                        }
                    }


                }
                else
                {
                    byte[] temp = files[oldName];

                    files.Remove(oldName);
                    files.Add(newName, temp);
                }
            }

            return NtStatus.Success;
        }
        //OD_OT

        //TO_DO
        public NtStatus ReadFile(string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info)
        {
            bytesRead = 0;
            info.Context = DokanNet.FileAccess.ReadData;

            bool t = info.Context.Equals((object)DokanNet.FileAccess.ReadData);
            if (t)
            {

               

                byte[] file = files[fileName];
                MemoryStream ms = new MemoryStream(file);
                //int i = 0; // Must be here becouse out int bytesRead

                lock (ms)
                {
                    ms.Read(buffer, 0, file.Length < buffer.Length ? file.Length : buffer.Length);
                    bytesRead = file.Length < buffer.Length ? file.Length : buffer.Length;
                }

                //for (i = 0; i < file.Length && i < buffer.Length; ++i)
                //    buffer[i] = file[i];

                //bytesRead = i;
            }


            return NtStatus.Success;
        }
        //OD_OT

        public NtStatus SetAllocationSize(string fileName, long length, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus SetEndOfFile(string fileName, long length, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus SetFileAttributes(string fileName, FileAttributes attributes, DokanFileInfo info)
        {
            return NtStatus.Success;
            // Maybe something here?
        }

        public NtStatus SetFileSecurity(string fileName, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus SetFileTime(string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus UnlockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            return NtStatus.Error;
        }

        public NtStatus Unmounted(DokanFileInfo info)
        {
            return NtStatus.Success;
        }

        //TO_DO
        public NtStatus WriteFile(string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info)
        {
            bytesWritten = 0;


            
            if (buffer.Length < MAX_FILE_SIZE * MB) 
            {
                journal.Enqueue((DateTime.Now.ToString(), fileName, START)); // Nothing important right now

                MemoryStream ms = new MemoryStream(buffer);

                lock (ms)
                {
                    byte[] file = new byte[buffer.Length];

                    ms.Read(file, 0, buffer.Length);

                    files[fileName] = file;
                    bytesWritten = buffer.Length;
                    bytes += buffer.Length;
                }

                //byte[] file = new byte[buffer.Length];

                //int i = 0; // Must be here becouse out int bytesWritten

                //for (i = 0; i < buffer.Length; ++i)
                //    file[i] = buffer[i];

                //files[fileName] = file;
                //bytesWritten = i;
                
                

                

                journal.Enqueue((DateTime.Now.ToString(), fileName, FINISHED)); //...
                return NtStatus.Success;
            }

            

            return NtStatus.Error;
        }
        //OD_OT



        private bool IsDirectory(string path)
        {
            return directorys.Contains(path);
        }

        private int GetCurrentDepth(string path)
        {
            return path.Split(DELIMITER.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}
